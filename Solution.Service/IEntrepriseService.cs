﻿using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Service
{
    public interface IEntrepriseService : IService<Entreprise>
    {
        IEnumerable<Entreprise> GetEntrepriseById(int id);

        void UpdateStatus(Entreprise doc);

        IEnumerable<Entreprise> RequestManagement();
        IEnumerable<Entreprise> listAcceptedRegister();
        int EntrepriseRequest();
        int EntrepriseAccepted();

    }
}
