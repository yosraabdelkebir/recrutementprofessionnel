﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Solution.Domain.Entities
{
    public class Certification
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string subject { get; set; }
        public string description { get; set; }
        public int? CandidatId { get; set; }
        [ForeignKey("CandidatId")]
        public virtual Candidate candidat { get; set; }
    }
}