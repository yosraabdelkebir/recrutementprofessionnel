﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Solution.Presentation.Models
{
    public class CandidateVM
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "First Name")]
        public string firstName { get; set; }
        [Display(Name = "Last Name")]
        public string lastName { get; set; }
        [Display(Name = "User Name")]
        public string userName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [MinLength(8)]
        [Display(Name = "Password")]
        public string password { get; set; }
/*
        [Required]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string confirmPassword { get; set; }*/

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string email { get; set; }

        [Required]
        [Display(Name = "Phone Number")]
        public string phoneNumber { get; set; }
        [Display(Name = "Introduction")]
        public string introduction { get; set; }
        [Display(Name = "Skills")]
        public string skills { get; set; }

        [Required]
        [Display(Name = "Address")]
        public string address { get; set; }
        [Display(Name = "Photo")]
        public string photo { get; set; }
        public int amis { get; set; } = 1;

        /*[DataType(DataType.Date)]
        public DateTime creationDate { get; set; }
        public int act  { get; set; }*/
    }
}