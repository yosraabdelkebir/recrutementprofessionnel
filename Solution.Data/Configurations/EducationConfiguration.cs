﻿using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Data.Configurations
{
    public class EducationConfiguration : EntityTypeConfiguration<Education>
    {
        public EducationConfiguration()
        {

            HasOptional(edu => edu.candidat).
                WithMany(cand => cand.Educations).
                HasForeignKey(edu => edu.CandidatId).
                WillCascadeOnDelete(true);
        }
    }
}
