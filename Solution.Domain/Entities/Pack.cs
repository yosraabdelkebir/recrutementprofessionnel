﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Domain.Entities
{
    public class Pack
    {
        [Key]
        public int PackId { get; set; }
        public float tauxReduction { get; set; }
    }
}
