﻿using Service.Pattern;
using Solution.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Service
{
    public class EntrepriseService:Service<Entreprise>,IEntrepriseService
    {
        static IDataBaseFactory factory = new DataBaseFactory();
        static IUnitOfWork UTK = new UnitOfWork(factory);
        IDataBaseFactory factor = null;
      



        public EntrepriseService():base(UTK)
        {

        }


        public void UpdateStatus(Entreprise entreprise)
        {
            UTK.GetRepositoryBase<Entreprise>().Update(entreprise);
            UTK.commit();
        }

        public int EntrepriseRequest()
        {
            return GetMany().Where(t => (t.EmailConfirmed == false) && (t.Role.Equals("entreprise"))).Count();
        }

        public int EntrepriseAccepted()
        {
            return GetMany().Where(t => (t.EmailConfirmed == true) && (t.Role.Equals("entreprise"))).Count();
        }


        public IEnumerable<Entreprise> RequestManagement()
        {

            return GetMany().Where(t => (t.EmailConfirmed == false) && (t.Role.Equals("entreprise"))).ToList();
        }

        public IEnumerable<Entreprise> listAcceptedRegister()
        {
            return GetMany().Where(t => (t.EmailConfirmed == true) && (t.Role.Equals("entreprise"))).ToList();
        }




        IEnumerable<Entreprise> IEntrepriseService.GetEntrepriseById(int id)
        {
            return null;
        }
    }
}
