﻿using Service.Pattern;
using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Service
{
    public interface ISubscriptionService : IService<Subscription>
    {
        IEnumerable<Subscription> GetAll();
        IEnumerable<Subscription> GetSubscriptionByIdFollower(int IdF);
        IEnumerable<Subscription> GetSubscriptionByIdEntreprise(int IdE);
    }
}
