﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Domain.Entities
{
    public class Contact
    {
        [Key, Column(Order=0)]
        public int SenderId { get; set; }
        [Key, Column(Order = 1)]
        public int ReceiverId { get; set; }
        [Key, Column(Order = 2)]
        public DateTime DateEnvoie { get; set; } = DateTime.Now;
        public int accept { get; set; } = 0;
        [ForeignKey("SenderId")]
        public virtual Candidate Sender { get; set; }
        [ForeignKey("ReceiverId")]
        public virtual Candidate Receiver { get; set; }
    }
}
