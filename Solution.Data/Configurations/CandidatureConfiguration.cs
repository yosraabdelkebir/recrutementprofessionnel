﻿using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Data.Configurations
{
    public class CanditatureConfiguration : EntityTypeConfiguration<Candidature>
    {
       public CanditatureConfiguration()
        {
            HasOptional(h => h.candidate).
                 WithMany(cond => cond.Candidatures).
                 HasForeignKey(h => h.CandidatId).
                 WillCascadeOnDelete(false);

        }
        

    }
}
