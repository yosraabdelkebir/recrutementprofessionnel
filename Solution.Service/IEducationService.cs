﻿using Service.Pattern;
using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Service
{
    public interface IEducationService : IService<Education>
    {
        //méthodes spécifiques (sauf CRUD)
        IEnumerable<Education> GetEducationByCandidate(int IdCandidate);
        IEnumerable<Education> GetEducationById(int Id);
    }
}
