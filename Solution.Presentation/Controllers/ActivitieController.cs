﻿using Solution.Data;
using Solution.Domain.Entities;
using Solution.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using webuser.Excel;
using webuser.Models;

namespace webuser.Controllers
{
    public class ActivitieController : Controller
    {
        private MyContext db;
        IActivityService serviceActivity = new ActivityService();
        IEntrepriseService serviceEntreprise = new EntrepriseService();
        // GET: Activity
        public ActionResult Index()
        {
            List<ActivityModel> list = new List<ActivityModel>();
            var idConnected = Int32.Parse(Session["Id"].ToString());
            foreach (var item in serviceActivity.GetMany().Where(t => t.enterpriseId == idConnected))
            {
                //ViewBag.userId = _userApplication.GetUserId(HttpContext.User);
                ActivityModel activity = new ActivityModel();
                if (item.roles == role.Conference)
                {
                    activity.type = "Conference";
                }
                if (item.roles == role.Evenment)
                {
                    activity.type = "Event";
                }
                if (item.roles == role.Workshop)
                {
                    activity.type = "Workshop";
                }
                activity.Id = item.id;

                activity.name = item.name;
                activity.dateEvent = item.dateEvent;
                activity.place = item.place;
               // activity.nbrPlaces = item.nbrplace;
                list.Add(activity);
            }
            return View(list);
        }


        public List<ActivityModel> Getall()
        {
            List<ActivityModel> list = new List<ActivityModel>();
            foreach (var item in serviceActivity.GetMany())
            {
                //ViewBag.userId = _userApplication.GetUserId(HttpContext.User);
                ActivityModel activity = new ActivityModel();
                if (item.roles == role.Conference)
                {
                    activity.type = "Conference";
                }
                if (item.roles == role.Evenment)
                {
                    activity.type = "Event";
                }
                if (item.roles == role.Workshop)
                {
                    activity.type = "Workshop";
                }
                activity.name = item.name;
                activity.dateEvent = item.dateEvent;
                activity.place = item.place;
                // activity.nbrPlaces = item.nbrplace;
                list.Add(activity);
            }
            return list;
        }





        public ActionResult ListActivities()
        {

            List<ActivityModel> list = new List<ActivityModel>();
            foreach (var item in serviceActivity.GetMany())
            {
                //ViewBag.userId = _userApplication.GetUserId(HttpContext.User);
                ActivityModel activity = new ActivityModel();
                if (item.roles == role.Conference)
                {
                    activity.type = "Conference";
                }
                if (item.roles == role.Evenment)
                {
                    activity.type = "Event";
                }
                if (item.roles == role.Workshop)
                {
                    activity.type = "Workshop";
                }
                activity.Id = item.id;
                activity.enterpriseId = (int)item.enterpriseId;
                activity.nameEntreprise = serviceEntreprise.GetById(activity.enterpriseId).Nom;
                activity.name = item.name;
                activity.dateEvent = item.dateEvent;
                activity.place = item.place;
               // activity.nbrPlaces = item.nbrplace;
                list.Add(activity);
            }
            return View(list);
        }

        public ActionResult Participate(int id)
        {
            Activite act = serviceActivity.GetById(id);
         //   act.nbrparticipate++;
            serviceActivity.UpdateStatus(act);
            try
            {


                MailMessage message = new MailMessage("recrutverif@gmail.com", "nasr.ladib@esprit.tn", "ekher nhar ", "L mandat mté3ék wfé ya baba");
                message.IsBodyHtml = true;
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential("recrutverif@gmail.com", "verif123456789");
                client.Send(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            return RedirectToAction("ListActivities", "Activity");
        }

        // GET: Activity/Details/5
        public ActionResult Details(int id)
        {
            Activite act = serviceActivity.GetById(id);
            ActivityModel actModel = new ActivityModel();
            actModel.Id = id;
            actModel.name = act.name;
            actModel.enterpriseId = (int)act.enterpriseId;
            actModel.nameEntreprise = serviceEntreprise.GetById(actModel.enterpriseId).Nom;
           // actModel.nbrPlaces = act.nbrplace;
            //actModel.nbrparticipate = act.nbrparticipate;
            if (act.roles == role.Conference)
            {
                actModel.type = "Conference";
            }
            if (act.roles == role.Evenment)
            {
                actModel.type = "Event";
            }
            if (act.roles == role.Workshop)
            {
                actModel.type = "Workshop";
            }

            actModel.description = act.description;


            return View(actModel);
        }



        public void Excel()
        {

            EventExcel excel = new EventExcel();
            Response.ClearContent();
            Response.BinaryWrite(excel.GenerateExcel(Getall()));
            Response.AddHeader("content-disposition", "attachment; filename=Activity.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Flush();
            Response.End();

        }


        // GET: Activity/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Activity/Create
        [HttpPost]
        public ActionResult Create(ActivityModel collection)
        {
            var idConnected = Int32.Parse(Session["Id"].ToString());
            Activite activity = new Activite
                {
                    dateEvent = collection.dateEvent,
                    description = collection.description,
                    name = collection.name,
                    place = collection.place,
                    roles = role.Evenment,
                    enterpriseId = idConnected,
                };

                serviceActivity.Add(activity);
                serviceActivity.Commit();
            int identreprise = (int)activity.enterpriseId;
            try
            {


                MailMessage message = new MailMessage("recrutverif@gmail.com", serviceEntreprise.GetById(identreprise).Email, "New Event ", "You have a new meeting planned, check with the HR");
                message.IsBodyHtml = true;
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential("recrutverif@gmail.com", "verif123456789");
                client.Send(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }



            return RedirectToAction("Index");
                 }

        // GET: Activity/Edit/5
        public ActionResult Edit(int id)
        {
            Activite oldActivity = serviceActivity.GetById(id);
            ActivityModel actModel = new ActivityModel{ name=oldActivity.name,dateEvent=oldActivity.dateEvent,place=oldActivity.place
          ,description=oldActivity.description };
            return View(actModel);
        }

        // POST: Activity/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ActivityModel collection)
        {
            try
            {
                Activite oldActivity = serviceActivity.GetById(id);

                // TODO: Add update logic here

                oldActivity.dateEvent = collection.dateEvent;
                oldActivity.description = collection.description;
                oldActivity.name = collection.name;
                oldActivity.place = collection.place;
                    
              

                serviceActivity.Update(oldActivity);
                serviceActivity.Commit();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Activity/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Activity/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
