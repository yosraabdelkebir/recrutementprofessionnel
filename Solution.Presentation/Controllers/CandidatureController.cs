﻿using Solution.Domain.Entities;
using Solution.Presentation.Models;
using Solution.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Solution.Presentation.Controllers
{
    public class CandidatureController : Controller
    {
        ICandidatureService Service = null;
        public static int idConnect = 5;

        public CandidatureController()
        {
            Service = new CandidatureService();
        }
        // GET: Candidature
        public ActionResult Index()
        {
            var candidats = new List<CandidatureVM>();
            foreach (Candidature cdvm in Service.GetMany())
            {
                candidats.Add(new CandidatureVM()
                {
                    CandidatureId =cdvm.CandidatureId,
                    nom = cdvm.nom,
                    prenom = cdvm.prenom,
                    pseudo = cdvm.pseudo,
                    num = cdvm.num,
                    intro = cdvm.intro,
                    skill = cdvm.skill,
                    addresse = cdvm.addresse,
                    duree = cdvm.duree
                });
            }
            return View(candidats);
            return View(Service.GetMany());
        }

        // GET: Candidature/Details/5
        public ActionResult Details(int id)
        {

         

            Candidature cdomain = Service.GetById(id);
            var candidat = new CandidatureVM()
            {
                CandidatureId = cdomain.CandidatureId,
                nom = cdomain.nom,
                prenom = cdomain.prenom,
                pseudo = cdomain.pseudo,
                num = cdomain.num,
                intro = cdomain.intro,
                skill = cdomain.skill,
                addresse = cdomain.addresse,
                duree = cdomain.duree,
            };

            return View(candidat);
        }


        // GET: Candidature/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Candidature/Create
        [HttpPost]
        public ActionResult Create(CandidatureVM cvm)
        {
            Candidature CandiBD = new Candidature()
            {
                nom = cvm.nom,
                prenom = cvm.prenom,
                pseudo = cvm.pseudo,
                num = cvm.num,
                intro = cvm.intro,
                skill = cvm.skill,
                addresse = cvm.addresse,
                duree = cvm.duree,


                

            };
            Service.Add(CandiBD);
            Service.Commit();
            
            return View();
        }

        // GET: Candidature/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Candidature/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Candidature/Delete/5
        public ActionResult Delete(int id)
        {
            if (id == null)

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Candidature cdm = Service.GetById(id);
            CandidatureVM cvm = new CandidatureVM()
            {
                CandidatureId = cdm.CandidatureId,
                nom = cdm.nom,
                prenom = cdm.prenom,
                pseudo = cdm.pseudo,
                num = cdm.num,
                intro = cdm.intro,
                skill = cdm.skill,
                addresse = cdm.addresse,
                duree = cdm.duree,
            };
            if (cdm == null)
                return HttpNotFound();
            return View(cvm);
        }


        // POST: Candidature/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (id == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Candidature cdm = Service.GetById(id);


                    if (cdm == null)
                        return HttpNotFound();

                    Service.Delete(cdm);
                    Service.Commit();

                    return RedirectToAction("Index");
                }

                return View();

            }
            catch
            {
                return RedirectToAction("Details", new { id = CandidateController.idConnect });
            }
        }
    }
}
