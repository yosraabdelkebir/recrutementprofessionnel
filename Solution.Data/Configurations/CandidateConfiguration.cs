﻿using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Data.Configurations
{
    public class CandidateConfiguration : EntityTypeConfiguration<Candidate>
    {
        public CandidateConfiguration()
        {

            HasMany(cand => cand.Entreprises).
                WithMany(ent => ent.Candidates).
                Map(M =>
                {
                    M.ToTable("Abonnements");
                    M.MapRightKey("Candidate");//right win ena
                    M.MapLeftKey("Entreprise");
                });
        }
    }
}
