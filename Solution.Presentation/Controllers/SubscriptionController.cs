﻿using Solution.Domain.Entities;
using Solution.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Solution.Presentation.Controllers
{
    public class SubscriptionController : Controller
    {
        ISubscriptionService subService = null;
        public SubscriptionController()
        {
            subService = new SubscriptionService();
        }

        // GET: Subscription
        public ActionResult Index()
        {
            return View();
        }

        // GET: Subscription/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Subscription/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Subscription/Create
        [HttpPost]
        public ActionResult Create(int id) //id passé en parametre est id de entreprise
        {
            Subscription SubscriptionBD = new Subscription()
            {
                FollowerId = CandidateController.idConnect,
                EntrepriseId = id
            };
            subService.Add(SubscriptionBD);
            subService.Commit();

            return RedirectToRoute("Index", "Candidate");
        }


        public ActionResult followEntreprise(int id) //id passé en parametre est id de entreprise
        {
            Subscription SubscriptionBD = new Subscription()
            {
                FollowerId = CandidateController.idConnect,
                EntrepriseId = id
            };
            subService.Add(SubscriptionBD);
            subService.Commit();

            return RedirectToAction("Index", "Candidate", new { SearchEntreprise = "" });
        }



        // GET: Subscription/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Subscription/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Subscription/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Subscription/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
