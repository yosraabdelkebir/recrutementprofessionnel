﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Domain.Entities
{
    public class Candidate
    {
        [Key]
        public int Id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string userName { get; set; }

        
        [Required]
        [DataType(DataType.Password)]
        [MinLength(8)]
        public string password { get; set; }

       
        [Required]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }

        [Required]
        public string phoneNumber { get; set; }
        public string introduction { get; set; }
        public string skills { get; set; }

        [Required]
        public string address { get; set; }
        public string photo { get; set; }
        [DataType(DataType.Date)]
        public DateTime creationDate { get; set; } = DateTime.Now;
        public int act { get; set; } = 1;

        public virtual ICollection<Education> Educations { get; set; }
        public virtual ICollection<Experience> Experiences { get; set; }
        public virtual ICollection<Certification> Certifications { get; set; }
        public virtual ICollection<Entreprise> Entreprises { get; set; }
        public virtual ICollection<Candidate> CandidateSender { get; set; }
        public virtual ICollection<Candidate> CandidateReceiver { get; set; }
        public virtual ICollection<Candidature> Candidatures { get; set; }
    }
}
