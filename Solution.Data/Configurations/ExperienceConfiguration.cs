﻿using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Data.Configurations
{
    public class ExperienceConfiguration: EntityTypeConfiguration<Experience>
    {
        public ExperienceConfiguration()
        {
            
            HasOptional(exp => exp.candidat).
                WithMany(cand => cand.Experiences).
                HasForeignKey(exp => exp.CandidatId).
                WillCascadeOnDelete(true);
        }
    }
}
