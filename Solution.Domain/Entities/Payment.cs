﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Domain.Entities
{
    public class Payment
    {
        [Key]
        public int PaymentId { get; set; }
        public float montant { get; set; }
    }
}
