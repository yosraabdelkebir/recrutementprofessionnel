﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Solution.Presentation.Models
{
    public class EntrepriseVM
    {
        [Key]
        public int id { get; set; }
        public String Nom { get; set; }
        public String Address { get; set; }
        public String UrlImage { get; set; }
        [DataType(DataType.Date)]
        public DateTime dateCreation { get; set; }
        public String fondateur { get; set; }
        public String categorie { get; set; }
    }
}