﻿using Solution.Data.Configurations;
using Solution.Data.CustomConventions;
using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Data
{
    public class MyContext:DbContext
    {
        public MyContext():base("name=MaChaine")
        {

        }
        //DbSet<> ...

        public DbSet<Candidate> Candidates { get; set; }
        public DbSet<Education> Educations { get; set; }
        public DbSet<Certification> Certifications { get; set; }
        public DbSet<Experience> Experiences { get; set; }
        public DbSet<Entreprise> Entreprises { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Pack> Packs { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Historique> Historiques { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<Candidature> Candidatures { get; set; }
        public DbSet<Disponibilite> Disponibilites { get; set; }
        public DbSet<Entretien> Entretiens { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new EducationConfiguration());
            modelBuilder.Configurations.Add(new ExperienceConfiguration());
            modelBuilder.Configurations.Add(new CertificationConfiguration());
            modelBuilder.Configurations.Add(new CandidateConfiguration());
            modelBuilder.Configurations.Add(new EntretienConfiguration());
            modelBuilder.Conventions.Add(new DateTimeConvention());
        }

       
    }
}
