﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Service.Pattern;
using Solution.Data.Infrastructure;
using Solution.Domain.Entities;

namespace Solution.Service
{
    public class CertificationService : Service<Certification>, ICertificationService
    {
        static IDataBaseFactory factory = new DataBaseFactory();
        static IUnitOfWork utk = new UnitOfWork(factory);
        public CertificationService() : base(utk)
        {

        }

        IEnumerable<Certification> ICertificationService.GetCertificationById(int Id)
        {
            return GetMany(f => f.Id == Id);
        }
    }
    
}
