﻿using Service.Pattern;
using Solution.Data.Infrastructure;
using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Service
{
    public class SubscriptionService : Service<Subscription>, ISubscriptionService
    {
        static IDataBaseFactory factory = new DataBaseFactory();
        static IUnitOfWork utk = new UnitOfWork(factory);
        public SubscriptionService() : base(utk)
        {
        }

        public IEnumerable<Subscription> GetAll()
        {
            return GetMany();
        }

        public IEnumerable<Subscription> GetSubscriptionByIdFollower(int IdF)
        {
            return GetMany(f => f.FollowerId == IdF);
        }


        public IEnumerable<Subscription> GetSubscriptionByIdEntreprise(int IdE)
        {
            return GetMany(f => f.EntrepriseId == IdE);
        }


    }
}
