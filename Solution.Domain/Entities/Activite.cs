﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Domain.Entities
{
    public class Activite
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime dateEvent { get; set; }
        public string type { get; set; }
        public string picture { get; set; }
        public string place  { get; set; }
        public int? enterpriseId { get; set; }
        public Entreprise enterprise { get; set; }
        public role roles { get; set; }

    }
    public enum role { Evenment, Workshop, Conference}
}
