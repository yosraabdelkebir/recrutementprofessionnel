﻿using Service.Pattern;
using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Service
{
    public interface IContactService : IService<Contact>
    {
        IEnumerable<Contact> GetAll();
        Contact GetByIdSR(int s, int r);
        bool GetBySenderId(int id);
        bool GetByReceiverId(int id);
        string GetAmis(int idS, int idR);
    }
}
