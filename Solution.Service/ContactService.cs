﻿using Service.Pattern;
using Solution.Data.Infrastructure;
using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Service
{
    public class ContactService : Service<Contact>, IContactService
    {
        static IDataBaseFactory factory = new DataBaseFactory();
        static IUnitOfWork utk = new UnitOfWork(factory);
        public ContactService() : base(utk)
        {}

        public IEnumerable<Contact> GetAll()
        {
            return GetMany();
        }

        public string GetAmis(int idS, int idR)
        {
            var all = new List<Contact>(GetMany());
            var cont = new List<Contact>();
            foreach (Contact c in all)
            {
                if (c.SenderId.Equals(idS)&&c.ReceiverId.Equals(idR))
                {
                    if (c.accept == 1)
                        return "oui";
                    return "enAtt";
                }
            }
            return "non";
           
        }

        public Contact GetByIdSR(int s, int r)
        {
            var all = new List<Contact>(GetMany());
            var cont = new Contact();
            foreach (Contact c in all)
            {
                if (c.SenderId.Equals(s) && c.ReceiverId.Equals(r))
                {
                    cont = c;
                }
            }
            return cont;
        }

        public bool GetByReceiverId(int id)
        {
            var all = new List<Contact>(GetMany());
            var cont = new List<Contact>();
            foreach (Contact c in all)
            {
                if (c.ReceiverId.Equals(id))
                {
                    return true;
                }
            }
            return false;
        }

        public bool GetBySenderId(int id)
        {
            var all = new List<Contact>(GetMany());
            var cont = new List<Contact>();
            foreach (Contact c in all)
            {
                if (c.SenderId.Equals(id))
                {
                    return true;
                }
            }    
            return false;
        }
    }
}
