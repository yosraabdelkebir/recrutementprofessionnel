﻿using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Presentation.Models
{
   
    public class EntretienVM

    {
        [Key]
        public int EntretienId { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime dateEntretien { get; set; }


        public int? CandidatureId { get; set; }
        
    }
}
