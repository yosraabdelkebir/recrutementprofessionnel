﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Domain.Entities
{
    public class Notification
    {
        [Key]
        public int NotificationId { get; set; }
        public string Contenu { get; set; }
        public DateTime dateEnvoi { get; set; }
        public int etat { get; set; }
    }
}
