﻿using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

public class Entreprise:User
{
   
    public String Nom { get; set; }
    public String Address { get; set; }
    public String  UrlImage { get; set; }
    [DataType(DataType.Date)]
    public DateTime dateCreation { get; set; }
    public String fondateur { get; set; }
    public String categorie { get; set; }
  
    public ICollection<String> projet { get; set; }
    public int? RHId { get; set; }
    public virtual RH RH { get; set; }
    public virtual ICollection<OffreEmploi> offers { get; set; }
    public virtual ICollection<Activite> Activites { get; set; }
    // public virtual ICollection<ChefProjet> ChefProjets { get; set; }





}
