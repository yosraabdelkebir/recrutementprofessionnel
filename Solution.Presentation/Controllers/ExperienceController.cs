﻿using Solution.Domain.Entities;
using Solution.Presentation.Models;
using Solution.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Solution.Presentation.Controllers
{
    public class ExperienceController : Controller
    {

        IExperienceService ExpService = null;
        public ExperienceController()
        {
            ExpService = new ExperienceService();
        }

        // GET: Experience
        public ActionResult Index()
        {
            //return View(Service.GetMany());
            var experiences = new List<ExperienceVM>();
            foreach (Experience edomain in ExpService.GetMany())
            {
                experiences.Add(new ExperienceVM()
                {
                    Id = edomain.Id,
                    workPlace = edomain.workPlace,
                    campany = edomain.campany,
                    description = edomain.description,
                    startDate = edomain.startDate,
                    endDate = edomain.endDate,
                    CandidatId = edomain.CandidatId
                });
            }
            return View(experiences);
        }

        // GET: Experience/Details/5
        public ActionResult Details(int id)
        {
            Experience e = ExpService.GetById(id);
            if (e == null)
            {
                return HttpNotFound();
            }

            return View(e);
        }

        // GET: Experience/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Experience/Create
        [HttpPost]
        public ActionResult Create(int id, ExperienceVM evm)
        {
            int result = DateTime.Compare(evm.startDate, evm.endDate);

            if (result == 0 || result > 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Experience experiencedomain = new Experience()
            {
                workPlace = evm.workPlace,
                campany = evm.campany,
                description = evm.description,
                startDate = evm.startDate,
                endDate = evm.endDate,
                CandidatId = id

            };
            ExpService.Add(experiencedomain);
            ExpService.Commit();

            return RedirectToAction("Details", "Candidate", new { id = CandidateController.idConnect });

        }

        // GET: Experience/Edit/5
        public ActionResult Edit(int id)
        {
            if (id==null)

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Experience edm = ExpService.GetById(id);
            ExperienceVM evm = new ExperienceVM()
            {
                Id = edm.Id,
                workPlace = edm.workPlace,
                campany = edm.campany,
                description = edm.description,
                startDate = edm.startDate,
                endDate = edm.endDate,
                CandidatId = edm.CandidatId
            };
            if (edm == null)
                return HttpNotFound();
            return View(evm);
        }

        // POST: Experience/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ExperienceVM evm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (id==null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    int result = DateTime.Compare(evm.startDate, evm.endDate);

                    if (result == 0 || result > 0)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Experience edm = ExpService.GetById(id);

                    edm.workPlace = evm.workPlace;
                    edm.campany = evm.campany;
                    edm.description = evm.description;
                    edm.startDate = evm.startDate;
                    edm.endDate = evm.endDate;
                    edm.CandidatId = evm.CandidatId;

                    if (edm == null)
                        return HttpNotFound();

                    ExpService.Update(edm);
                    ExpService.Commit();

                    return RedirectToAction("Details", "Candidate", new { id = CandidateController.idConnect });
                }

                return RedirectToAction("Details", "Candidate", new { id = CandidateController.idConnect });

            }
            catch
            {
                return RedirectToAction("Details", "Candidate", new { id = CandidateController.idConnect });
            }
        }

        // GET: Experience/Delete/5
        public ActionResult Delete(int id)
        {
            if (id == null)

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Experience edm = ExpService.GetById(id);
            ExperienceVM evm = new ExperienceVM()
            {
                Id = edm.Id,
                workPlace = edm.workPlace,
                campany = edm.campany,
                description = edm.description,
                startDate = edm.startDate,
                endDate = edm.endDate,
                CandidatId = edm.CandidatId
            };
            if (edm == null)
                return HttpNotFound();
            return View(evm);

        }

        // POST: Experience/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (id == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Experience edm = ExpService.GetById(id);


                    if (edm == null)
                        return HttpNotFound();

                    ExpService.Delete(edm);
                    ExpService.Commit();

                    return RedirectToAction("Details", "Candidate", new { id = CandidateController.idConnect });
                }

                return View();

            }
            catch
            {
                return View();
            }
        }
    }
}
