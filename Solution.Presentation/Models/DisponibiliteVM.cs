﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Solution.Presentation.Models
{
    public class DisponibiliteVM
    {
        [Key]
        [DataType(DataType.Date)]
        public DateTime datedis { get; set; }
        public int Id { get; set; }
    }
}