﻿using Solution.Domain.Entities;
using Solution.Presentation.Models;
using Solution.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace Solution.Presentation.Controllers
{
    public class CandidateController : Controller
    {
        ICandidateService candidateService = null;
        IContactService contactService = null;
        ICertificationService certificationService = null;
        IEducationService educationService = null;
        IExperienceService experienceService = null;
        IEntrepriseService entrepriseService = null;
        public static int idConnect = 0;
        public CandidateController()
        {
            candidateService = new CandidateService();
            contactService = new ContactService();
            certificationService = new CertificationService();
            experienceService = new ExperienceService();
            entrepriseService = new EntrepriseService();
            educationService = new EducationService();
        }

       
        public ActionResult loginCandidate()
        {
            return View();
        }
        [HttpPost]
        public ActionResult loginCandidate(string email, string password)
        {
            var allCandidate = new List<Candidate>(candidateService.GetMany());
            foreach (Candidate c in allCandidate)
            {
                if ((c.email.Equals(email)) && (c.password.Equals(password)))
                {
                    idConnect = c.Id;
                }
            }
            return RedirectToAction("Details", "Candidate", new { id = CandidateController.idConnect });

        }
        // GET: Candidate
        public ActionResult Index(string SearchString, string SearchEntreprise, int? page)
        { 
            /*******/
            var entreprises = new List<EntrepriseVM>();
            var allEntreprise = new List<Entreprise>(entrepriseService.GetMany());
            var entrepriseView = new List<Entreprise>(entrepriseService.GetMany());
            if (!String.IsNullOrEmpty(SearchEntreprise))
            {
                entrepriseView = new List<Entreprise>();
                foreach (Entreprise c in allEntreprise)
                {
                    if (c.Nom.ToLower().Contains(SearchEntreprise.ToLower()) ||
                        c.categorie.ToLower().Contains(SearchEntreprise.ToLower()))
                    {
                        entrepriseView.Add(c);
                    }
                }
            }

            var Entreprises = new List<Entreprise>();
            foreach (Entreprise es in entrepriseView)
            {
                Entreprises.Add(es);
            }
            ViewBag.myEntreprises = new List<Entreprise>(Entreprises);
            /*******/

            var candidats = new List<CandidateVM>();
            var allCandidate = new List<Candidate>(candidateService.GetMany());
            var candidateView = new List<Candidate>(candidateService.GetMany());
            if (!String.IsNullOrEmpty(SearchString))
            {
                candidateView = new List<Candidate>();
                foreach (Candidate c in allCandidate)
                {
                    if (c.firstName.ToLower().Contains(SearchString.ToLower()) || 
                        c.lastName.ToLower().Contains(SearchString.ToLower()) || 
                        c.userName.ToLower().Contains(SearchString.ToLower()) ||
                        c.email.ToLower().Contains(SearchString.ToLower()))
                    {
                        candidateView.Add(c);
                    }
                }
            }

            var amis = 0;
            // 0 ne sont pas amis
            // -1 en att
            // 1 amis
            foreach (Candidate cdvm in candidateView)
            {
                if (contactService.GetAmis(idConnect,cdvm.Id) == "oui") amis = 1;
                else if (contactService.GetAmis(idConnect,cdvm.Id) == "enAtt") amis = -1;
                else amis = 0;
                if (cdvm.Id != idConnect)
                {



                    candidats.Add(new CandidateVM()
                    {
                        Id = cdvm.Id,
                        firstName = cdvm.firstName,
                        lastName = cdvm.lastName,
                        userName = cdvm.userName,
                        email = cdvm.email,
                        password = cdvm.password,
                        address = cdvm.address,
                        phoneNumber = cdvm.phoneNumber,
                        introduction = cdvm.introduction,
                        skills = cdvm.skills,
                        photo = cdvm.photo,
                        amis = amis
                    });
                }
            }
            return View(candidats.ToPagedList(page ?? 1, 3));
        }

        // GET: Candidate/Details/5
        public ActionResult Details(int id)
        {

            var Certifications = new List<Certification>();
            foreach (Certification c in certificationService.GetMany())
            {
                if (c.CandidatId == id)
                    Certifications.Add(c);
            }
            ViewBag.myCertifications = new List<Certification>(Certifications);


            var Educations = new List<Education>();
            foreach (Education ed in educationService.GetMany())
            {
                if (ed.CandidatId == id)
                    Educations.Add(ed);
            }
            ViewBag.myEducations = new List<Education>(Educations);


            var Experiences = new List<Experience>();
            foreach (Experience exp in experienceService.GetMany())
            {
                if (exp.CandidatId == id)
                    Experiences.Add(exp);
            }
            ViewBag.myExperiences = new List<Experience>(Experiences);


            Candidate cdomain = candidateService.GetById(id);
            var candidat = new CandidateVM()
                {
                    Id = cdomain.Id,
                    firstName = cdomain.firstName,
                    lastName = cdomain.lastName,
                    userName = cdomain.userName,
                    email = cdomain.email,
                    password = cdomain.password,
                    address = cdomain.address,
                    phoneNumber = cdomain.phoneNumber,
                    introduction = cdomain.introduction,
                    skills = cdomain.skills,
                    photo = cdomain.photo

            };
            
            return View(candidat);
        }


        // GET: Candidate/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Candidate/Create
        [HttpPost]
        public ActionResult Create(CandidateVM cdvm, HttpPostedFileBase file)
        {
            if (!ModelState.IsValid || file == null || file.ContentLength == 0)
            {
                RedirectToAction("Create");
            }
           
                Candidate CandidatBD = new Candidate()
                {
                    firstName = cdvm.firstName,
                    lastName = cdvm.lastName,
                    userName = cdvm.userName,
                    email = cdvm.email,
                    password = cdvm.password,
                    phoneNumber = cdvm.phoneNumber,
                    introduction = cdvm.introduction,
                    skills = cdvm.skills,
                    address = cdvm.address,
                    photo = file.FileName
                };
                candidateService.Add(CandidatBD);
                candidateService.Commit();
                var fileName = "";
                if (file.ContentLength > 0)
                {
                    fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/Uploads/"), fileName);
                    file.SaveAs(path);
                }

            GMailer.GmailUsername = "yosraabdelkebir16@gmail.com";
            GMailer.GmailPassword = "13459152aB";

            GMailer mailer = new GMailer();
            mailer.ToEmail = CandidatBD.email;
            mailer.Subject = "Find a job - New Candidate Account";
            mailer.Body = "Thanks for Registering your account, Welcome to Find Me A Job!";
            mailer.IsHtml = true;
            mailer.Send();


            return RedirectToAction("loginCandidate");
        }

        // GET: Candidate/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == null)

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Candidate cdm = candidateService.GetById(id);
            CandidateVM cvm = new CandidateVM()
            {
                Id = cdm.Id,
                firstName = cdm.firstName,
                lastName = cdm.lastName,
                userName = cdm.userName,
                password = cdm.password,
                email = cdm.email,
                phoneNumber = cdm.phoneNumber,
                introduction = cdm.introduction,
                skills = cdm.skills,
                address = cdm.address,
                photo = cdm.photo
            };
            if (cdm == null)
                return HttpNotFound();
            return View(cvm);
        }

        // POST: Candidate/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CandidateVM cvm/*, HttpPostedFileBase file*/)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (id == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Candidate cdm = candidateService.GetById(id);


                cdm.firstName = cvm.firstName;
                cdm.lastName = cvm.lastName;
                cdm.userName = cvm.userName;
                cdm.email = cvm.email;
                cdm.phoneNumber = cvm.phoneNumber;
                cdm.introduction = cvm.introduction;
                cdm.skills = cvm.skills;
                cdm.address = cvm.address;
                /*cdm.photo = file.FileName;
                    var fileName = "";
                    if (file.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/Content/Uploads/"), fileName);
                        file.SaveAs(path);
                    }*/

                    if (cdm == null)
                        return HttpNotFound();

                    candidateService.Update(cdm);
                    candidateService.Commit();

                    return RedirectToAction("Details", "Candidate", new { id = CandidateController.idConnect });
                }

                return View();

            }
            catch
            {
                return RedirectToAction("Index", "Candidate", new { SearchString = "", SearchEntreprise = "" });
            }
        }

        // GET: Candidate/Delete/5
        public ActionResult Delete(int id)
        {
            if (id == null)

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Candidate cdm = candidateService.GetById(id);
            CandidateVM cvm = new CandidateVM()
            {
                Id = cdm.Id,
                firstName = cdm.firstName,
                lastName = cdm.lastName,
                userName = cdm.userName,
                password = cdm.password,
                email = cdm.email,
                phoneNumber = cdm.phoneNumber,
                introduction = cdm.introduction,
                skills = cdm.skills,
                address = cdm.address,
                photo = cdm.photo
            };
            if (cdm == null)
                return HttpNotFound();
            return View(cvm);
        }

        // POST: Candidate/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (id == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Candidate cdm = candidateService.GetById(id);


                    if (cdm == null)
                        return HttpNotFound();

                    candidateService.Delete(cdm);
                    candidateService.Commit();

                    return RedirectToAction("Index", "Candidate", new { SearchString = "", SearchEntreprise = "" });
                }

                return View();

            }
            catch
            {
                return RedirectToAction("Details",new { id = CandidateController.idConnect });
            }
        }

        public ActionResult AddCertif(int id)
        {
            return RedirectToAction("Create", "Certification", new { id = id });
        }
        public ActionResult EditCertif(int id)
        {
            return RedirectToAction("Edit", "Certification", new { id = id });
        }

        public ActionResult DeleteCertif(int id)
        {
            return RedirectToAction("Delete", "Certification", new { id = id });
        }

        public ActionResult addContact(int id)
        {
            return RedirectToAction("followCandidate", "Contact", new { id = id });
        }

        public ActionResult RequestC(int id)
        {
            return RedirectToAction("Requests", "Contact", new { id = id });
        }

        public ActionResult deleteContact(int id)
        {
            return RedirectToAction("deleteContact", "Contact", new { id = id });
        }

        public ActionResult DetailsContact(int id)
        {

            var Certifications = new List<Certification>();
            foreach (Certification c in certificationService.GetMany())
            {
                if (c.CandidatId == id)
                    Certifications.Add(c);
            }
            ViewBag.myCertifications = new List<Certification>(Certifications);


            var Educations = new List<Education>();
            foreach (Education ed in educationService.GetMany())
            {
                if (ed.CandidatId == id)
                    Educations.Add(ed);
            }
            ViewBag.myEducations = new List<Education>(Educations);


            var Experiences = new List<Experience>();
            foreach (Experience exp in experienceService.GetMany())
            {
                if (exp.CandidatId == id)
                    Experiences.Add(exp);
            }
            ViewBag.myExperiences = new List<Experience>(Experiences);

            Candidate cdomain = candidateService.GetById(id);
            var candidat = new CandidateVM()
            {
                Id = cdomain.Id,
                firstName = cdomain.firstName,
                lastName = cdomain.lastName,
                userName = cdomain.userName,
                email = cdomain.email,
                password = cdomain.password,
                address = cdomain.address,
                phoneNumber = cdomain.phoneNumber,
                introduction = cdomain.introduction,
                skills = cdomain.skills,
                photo = cdomain.photo

            };

            return View(candidat);
        }
        public ActionResult MyContact(int id)
        {
            return RedirectToAction("MyContact", "Contact", new { id = id });
        }


        public ActionResult AddEdu(int id)
        {
            return RedirectToAction("Create", "Education", new { id = id });
        }
        public ActionResult EditEdu(int id)
        {
            return RedirectToAction("Edit", "Education", new { id = id });
        }

        public ActionResult DeleteEdu(int id)
        {
            return RedirectToAction("Delete", "Education", new { id = id });
        }

        public ActionResult AddExp(int id)
        {
            return RedirectToAction("Create", "Experience", new { id = id });
        }
        public ActionResult EditExp(int id)
        {
            return RedirectToAction("Edit", "Experience", new { id = id });
        }

        public ActionResult DeleteExp(int id)
        {
            return RedirectToAction("Delete", "Experience", new { id = id });
        }
        public ActionResult followEntreprise(int id)
        {
            return RedirectToAction("followCandidate", "Contact", new { id = id });
        }

        public ActionResult addEntreprise(int id)
        {
            return RedirectToAction("followEntreprise", "Subscription", new { id = id });
        }

        public ActionResult ReturnProfile()
        {
            return RedirectToAction("Details", new { id = idConnect });
        }


    }
}
