﻿using Solution.Domain.Entities;
using Solution.Presentation.Models;
using Solution.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Solution.Presentation.Controllers
{
    public class EntretienController : Controller
    {

        IEntretienService Service = null;
        public static int idConnect = 5;
        ICandidatureService applicationSercvice = null;


        public EntretienController()
        {
            Service = new EntretienService();
            applicationSercvice = new CandidatureService();
        }
        // GET: Entretien
        public ActionResult Index()
        {
            var candidats = new List<EntretienVM>();
            foreach (Entretien cdvm in Service.GetMany())
            {
                candidats.Add(new EntretienVM()
                {
                    EntretienId = cdvm.EntretienId,
                    dateEntretien = cdvm.dateEntretien,
                    CandidatureId = cdvm.CandidatureId,
                   
                });
            }
            return View(candidats);
            return View(Service.GetMany());
        }
        // GET: Entretien/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Entretien/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Entretien/Create
      
        [HttpPost]
        public ActionResult Create(EntretienVM cvm)
        {
            Entretien CandiBD = new Entretien()
            {
                dateEntretien = cvm.dateEntretien,
                CandidatureId = 1,
                




            };
            Service.Add(CandiBD);
            Service.Commit();

            return RedirectToAction("Index");
        }
        public ActionResult mail(int id)
        {
            GMailer.GmailUsername = "sarah.belkadhi@esprit.tn";
            GMailer.GmailPassword = "toutouwhisky";

            GMailer mailer = new GMailer();
            mailer.ToEmail = applicationSercvice.GetById(id).email;
            mailer.Subject = "Find a job - New Candidate Account";
            mailer.Body = "Thanks for Registering your account, Welcome to Find Me A Job!";
            mailer.IsHtml = true;
            mailer.Send();
            return RedirectToAction("Create", new { id = id });
        }

        // GET: Candidature

        // GET: Entretien/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == null)

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Entretien cdm = Service.GetById(id);
            EntretienVM cvm = new EntretienVM()
            {
                EntretienId = cdm.EntretienId,
                CandidatureId =cdm.CandidatureId,
                dateEntretien = cdm.dateEntretien,

            };
            if (cdm == null)
                return HttpNotFound();
            return View(cvm);
        }

        // POST: Entretien/Edit/5
        [HttpPost]

        public ActionResult Edit(int id, EntretienVM cvm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (id == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Entretien cdm = Service.GetById(id);


                    cdm.CandidatureId = cvm.CandidatureId;
                    cdm.dateEntretien = cvm.dateEntretien;
                    cdm.dateEntretien = cvm.dateEntretien;


                    if (cdm == null)
                        return HttpNotFound();

                    Service.Update(cdm);
                    Service.Commit();

                    return RedirectToAction("Index");
                }

                return View();

            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
        // GET: Entretien/Delete/5
        public ActionResult Delete(int id)
        {
            if (id == null)

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Entretien cdm = Service.GetById(id);
            EntretienVM cvm = new EntretienVM()
            {
                CandidatureId = cdm.CandidatureId,
                dateEntretien = cdm.dateEntretien,
                EntretienId = cdm.EntretienId,
               
            };
            if (cdm == null)
                return HttpNotFound();
            return View(cvm);
        }

        // POST: Entretien/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (id == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Entretien cdm = Service.GetById(id);


                    if (cdm == null)
                        return HttpNotFound();

                    Service.Delete(cdm);
                    Service.Commit();

                    return RedirectToAction("Index");
                }

                return View();

            }
            catch
            {
                return RedirectToAction("Details", new { IdEntretien = EntretienController.idConnect });
            }
        }
    }
}
