﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Service.Pattern;
using Solution.Data.Infrastructure;
using Solution.Domain.Entities;

namespace Solution.Service
{
    public class CandidatureService : Service<Candidature>, ICandidatureService
    {
        static IDataBaseFactory factory = new DataBaseFactory();
        static IUnitOfWork utk = new UnitOfWork(factory);
        public CandidatureService() : base(utk)
        {

        }

        public IEnumerable<Candidature> GetCandidatureById(int CandidatureId)
        {
            return GetMany(c => c.CandidatureId == CandidatureId);
        }
    }
}
