﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Service.Pattern;
using Solution.Data.Infrastructure;
using Solution.Domain.Entities;

namespace Solution.Service
{
    public class EducationService : Service<Education>, IEducationService
    {
        static IDataBaseFactory factory = new DataBaseFactory();
        static IUnitOfWork utk = new UnitOfWork(factory);

        public EducationService() : base(utk)
        {

        }

        public IEnumerable<Education> GetEducationById(int Id)
        {
            return GetMany(f => f.Id == Id);
        }

        public IEnumerable<Education> GetEducationByCandidate(int IdCandidate)
        {
            return GetMany(e => e.Id == IdCandidate);
        }

    }
}
