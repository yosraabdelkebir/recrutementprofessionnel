﻿using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Data.Configurations
{
   public class EntretienConfiguration : EntityTypeConfiguration<Entretien>
    {
        public EntretienConfiguration()
        {
            HasRequired(cer => cer.Candidature).
                  WithMany(cond => cond.Entretiens).
                  HasForeignKey(cer => cer.CandidatureId).
                  WillCascadeOnDelete(false);
        }
      
    }
}
