﻿

using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Domain.Entities
{



    public class Candidature

    {
        [Key]
        public int CandidatureId { get; set; }
        public string nom { get; set; }
        public string prenom { get; set; }
        public string pseudo { get; set; }
        public string email { get; set; }


        /*
        [NotMapped]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string confirmPassword { get; set; }
        */


        [Required]
        public string num { get; set; }
        public string intro { get; set; }
        public string skill { get; set; }

        [Required]
        public string addresse { get; set; }



        //public ICollection<Quiz> Quizs { get; set; }
        public int duree { get; set; }


        /*[ForeignKey("idQuiz")]
        public virtual Quiz Quiz { get; set; }*/
        /* [Column(Order = 2)]
         [ForeignKey("Offre")]
         public int OffreId { get; set; }
         public Offre  Offre { get; set; }*/
        public int? CandidatId { get; set; }

        [ForeignKey("CandidatId")]
        public virtual Candidate candidate { get; set; }

        public virtual ICollection<Entretien> Entretiens { get; set; }
        
    }
}
