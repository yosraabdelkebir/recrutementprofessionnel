﻿using Solution.Domain.Entities;
using Solution.Service;
using Solution.Presentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;

namespace Solution.Presentation.Controllers
{
    public class ContactController : Controller
    {

        IContactService contactService = null;
        ICandidateService candidateService = null;
        public ContactController()
        {
            contactService = new ContactService();
            candidateService = new CandidateService();
        }
        // GET: Contact
        public ActionResult Index()
        {
            return View();
        }

        // GET: Contact/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Contact/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Contact/Create
        [HttpPost]
        public ActionResult Create(int id)
        {
            Contact ContactBD = new Contact()
            {
                ReceiverId = id,
                SenderId = CandidateController.idConnect
            };
            contactService.Add(ContactBD);
            contactService.Commit();

            return RedirectToAction("Index", "Candidate", new { SearchString = "", SearchEntreprise = "" });
        }

        public ActionResult followCandidate(int id)
        {
            Contact ContactBD = new Contact()
            {
                ReceiverId = id,
                SenderId = CandidateController.idConnect
            };
            contactService.Add(ContactBD);
            contactService.Commit();
            return RedirectToAction("Index", "Candidate", new { SearchString = "", SearchEntreprise = "" });
        }

        // GET: Contact/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Contact/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Contact/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Contact/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Requests(int id)
        {
            var all = new List<Contact>(contactService.GetMany());
            var enAtt = new List<ContactVM>();
            var enAttCand = new List<CandidateVM>();
            foreach (Contact c in all)
            {
                if (c.ReceiverId == id)
                {
                    if (c.accept == 0)
                        enAtt.Add(new ContactVM()
                        { ReceiverId=c.ReceiverId,
                          SenderId=c.SenderId,
                          DateEnvoie=c.DateEnvoie,
                          accept=c.accept
                        });
                }
            }
            foreach (Candidate cdvm in candidateService.GetMany())
            {   foreach (ContactVM c in enAtt)
                {
                    if (c.SenderId == cdvm.Id)
                    {
                        enAttCand.Add(new CandidateVM() {
                            Id=cdvm.Id,
                            firstName = cdvm.firstName,
                            lastName = cdvm.lastName,
                            userName = cdvm.userName,
                            email = cdvm.email,
                            password = cdvm.password,
                            phoneNumber = cdvm.phoneNumber,
                            introduction = cdvm.introduction,
                            skills = cdvm.skills,
                            address = cdvm.address,
                            photo = cdvm.photo,
                        });
                    }
                } 
            }
            ViewBag.myContactRequest = new List<CandidateVM>(enAttCand);
            return View(enAtt);
        }

        public ActionResult deleteContact(int id)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    if (id == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Contact cdm = contactService.GetByIdSR(CandidateController.idConnect,id);


                    if (cdm == null)
                        return HttpNotFound();

                    contactService.Delete(cdm);
                    contactService.Commit();


                    return RedirectToAction("Index", "Candidate", new { SearchString = "" , SearchEntreprise = ""});
                }

                return View("Details", "Candidate", new { id = CandidateController.idConnect });

            }
            catch
            {
                return RedirectToAction("Index");
            }
            
        }
        public ActionResult DetailsContact(int id)
        {
            return RedirectToAction("DetailsContact", "Candidate", new { id = id });
        }

        public ActionResult Accept(int id)
        {
            var toedit = new Contact();
            toedit = contactService.GetByIdSR(id, CandidateController.idConnect);
            toedit.accept = 1;
            contactService.Update(toedit);
            contactService.Commit();

            return RedirectToAction("Requests", new { id = CandidateController.idConnect });
        }

        public ActionResult Refuse(int id)
        {
            var toremove = new Contact();
            toremove = contactService.GetByIdSR(id, CandidateController.idConnect);
            contactService.Delete(toremove);
            contactService.Commit();

            return RedirectToAction("Requests", new { id = CandidateController.idConnect });
        }
        public ActionResult MyContact(int id)
        {
            var all = new List<Contact>(contactService.GetMany());
            var myContact = new List<ContactVM>();
            var myCandidate = new List<CandidateVM>();
            foreach (Contact c in all)
            {
                if (c.ReceiverId == id || c.SenderId==id)
                {
                    if (c.accept == 1)
                        myContact.Add(new ContactVM()
                        {
                            ReceiverId = c.ReceiverId,
                            SenderId = c.SenderId,
                            DateEnvoie = c.DateEnvoie,
                            accept = c.accept
                        });
                }
            }
            foreach (Candidate cdvm in candidateService.GetMany())
            {
                foreach (ContactVM c in myContact)
                {
                    if ((c.ReceiverId == cdvm.Id || c.SenderId == cdvm.Id)&& c.accept==1)
                    {
                        myCandidate.Add(new CandidateVM()
                        {
                            Id = cdvm.Id,
                            firstName = cdvm.firstName,
                            lastName = cdvm.lastName,
                            userName = cdvm.userName,
                            email = cdvm.email,
                            password = cdvm.password,
                            phoneNumber = cdvm.phoneNumber,
                            introduction = cdvm.introduction,
                            skills = cdvm.skills,
                            address = cdvm.address,
                            photo = cdvm.photo,
                        });
                    }
                }
            }
            ViewBag.myContacts = new List<CandidateVM>(myCandidate);
            return View(myContact);
        }
    }
}
