﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Solution.Presentation.Models
{
    public class SubscriptionVM
    {
        [Key, Column(Order = 0)]
        public int FollowerId { get; set; }
        [Key, Column(Order = 1)]
        public int EntrepriseId { get; set; }
        [Key, Column(Order = 2)]
        public DateTime DateEnvoie { get; set; } = DateTime.Now;
    }
}