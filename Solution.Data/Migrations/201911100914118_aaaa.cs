namespace Solution.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class aaaa : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Candidatures",
                c => new
                    {
                        CandidatureId = c.Int(nullable: false, identity: true),
                        nom = c.String(),
                        prenom = c.String(),
                        pseudo = c.String(),
                        num = c.String(nullable: false),
                        intro = c.String(),
                        skill = c.String(),
                        addresse = c.String(nullable: false),
                        duree = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CandidatureId);
            
            CreateTable(
                "dbo.Entretiens",
                c => new
                    {
                        EntretienId = c.Int(nullable: false, identity: true),
                        dateEntretien = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CandidatureId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EntretienId)
                .ForeignKey("dbo.Candidatures", t => t.CandidatureId)
                .Index(t => t.CandidatureId);
            
            CreateTable(
                "dbo.Disponibilites",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        datedis = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Entretiens", "CandidatureId", "dbo.Candidatures");
            DropIndex("dbo.Entretiens", new[] { "CandidatureId" });
            DropTable("dbo.Disponibilites");
            DropTable("dbo.Entretiens");
            DropTable("dbo.Candidatures");
        }
    }
}
