﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Solution.Presentation.Models
{
    public class EducationVM
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string diploma { get; set; }
        [Required]
        public string institution { get; set; }
        public string description { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Start Date : ")]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        
        public DateTime startDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime endDate { get; set; }
        public int? CandidatId { get; set; }
    }
}