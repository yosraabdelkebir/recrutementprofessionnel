﻿using Service.Pattern;
using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Service
{
    public interface IExperienceService : IService<Experience>
    {
        //méthodes spécifiques (sauf CRUD)
        IEnumerable<Experience> GetExperienceByCandidate(int IdCandidate);
        IEnumerable<Experience> GetExperienceById(int Id);
    }
}
