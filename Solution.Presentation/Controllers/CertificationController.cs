﻿using Solution.Domain.Entities;
using Solution.Presentation.Models;
using Solution.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Solution.Presentation.Controllers
{
    public class CertificationController : Controller
    {
        ICertificationService certificationService = null;
        public CertificationController()
        {
            certificationService = new CertificationService();
        }


        // GET: Certification
        public ActionResult Index()
        {
            return View();
        }

        // GET: Certification/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Certification/Create
        public ActionResult Create()
        { 
            return View();
        }

        // POST: Certification/Create
        [HttpPost]
        public ActionResult Create(int id,CertificationVM cvm)
        {

            Certification certiftBD = new Certification()
            {
                subject = cvm.subject,
                description = cvm.description,
                CandidatId = id
            };
            certificationService.Add(certiftBD);
            certificationService.Commit();

            return RedirectToAction("Details", "Candidate", new { id = CandidateController.idConnect });

        }

        // GET: Certification/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == null)

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Certification cdm = certificationService.GetById(id);
            CertificationVM cvm = new CertificationVM()
            {
                Id = cdm.Id,
                subject = cdm.subject,
                description = cdm.description,
                CandidatId = cdm.CandidatId
            };
            if (cdm == null)
                return HttpNotFound();
            return View(cvm);
        }

        // POST: Certification/Edit/5
        [HttpPost]
        public ActionResult Edit(int id,CertificationVM cvm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (id == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Certification cdm = certificationService.GetById(id);

                    cdm.subject = cvm.subject;
                    cdm.description = cvm.description;
                    cdm.CandidatId = cvm.CandidatId;

                    if (cdm == null)
                        return HttpNotFound();

                    certificationService.Update(cdm);
                    certificationService.Commit();

                    return RedirectToAction("Details", "Candidate", new { id = CandidateController.idConnect });
                }
                
                return RedirectToAction("Details", "Candidate", new { id = CandidateController.idConnect });

            }
            catch
            {
                return RedirectToAction("Details", "Candidate", new { id = CandidateController.idConnect });
            }
        }

        // GET: Certification/Delete/5
        public ActionResult Delete(int id)
        {
            if (id == null)

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Certification cdm = certificationService.GetById(id);
            CertificationVM cvm = new CertificationVM()
            {
                Id = cdm.Id,
                subject = cdm.subject,
                description = cdm.description,
                CandidatId = cdm.CandidatId
            };
            if (cdm == null)
                return HttpNotFound();
            return View(cvm);
        }

        // POST: Certification/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (id == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Certification cdm = certificationService.GetById(id);


                    if (cdm == null)
                        return HttpNotFound();

                    certificationService.Delete(cdm);
                    certificationService.Commit();

                    return RedirectToAction("Details", "Candidate", new { id = CandidateController.idConnect });
                }

                return View();

            }
            catch
            {
                return View();
            }
        }
    }
}
