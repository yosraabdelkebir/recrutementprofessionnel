namespace Solution.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class aaaa1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Candidatures", "CandidatId", c => c.Int());
            CreateIndex("dbo.Candidatures", "CandidatId");
            AddForeignKey("dbo.Candidatures", "CandidatId", "dbo.Candidates", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Candidatures", "CandidatId", "dbo.Candidates");
            DropIndex("dbo.Candidatures", new[] { "CandidatId" });
            DropColumn("dbo.Candidatures", "CandidatId");
        }
    }
}
