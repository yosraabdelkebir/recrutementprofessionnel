﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Solution.Presentation.Models
{
    public class CertificationVM
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string subject { get; set; }
        public string description { get; set; }
        public int? CandidatId { get; set; }

    }
}