﻿using Solution.Data;
using Solution.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using webuser.Models;

namespace webuser.Controllers
{
    public class AdminController : Controller
    {


        private MyContext db;
        IEntrepriseService serviceEntreprise = new EntrepriseService();
        IActivityService serviceActivity = new ActivityService();

        // GET: Admin

        public ActionResult Index()
        {
            return View();
        }

        // GET: Admin/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }


        public int acceptRequest(int id)
        {

            Entreprise ent = serviceEntreprise.GetById(id);
            ent.EmailConfirmed = true;
            serviceEntreprise.UpdateStatus(ent);
            Session["Accept"] = serviceEntreprise.EntrepriseAccepted().ToString();
            try
            {


                MailMessage message = new MailMessage("recrutverif@gmail.com", ent.Email, "Response", "Your request has been accepted");
                message.IsBodyHtml = true;
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential("recrutverif@gmail.com", "verif123456789");
                client.Send(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }

            return 1;
        }
        public int refuseRequest(int id)
        {

            Entreprise ent = serviceEntreprise.GetById(id);
            ent.EmailConfirmed = false;
            serviceEntreprise.UpdateStatus(ent);
            Session["Refused"] = serviceEntreprise.EntrepriseAccepted().ToString();
            try
            {


                MailMessage message = new MailMessage("recrutverif@gmail.com", ent.Email, "Response", "Your request has been rejected");
                message.IsBodyHtml = true;
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential("recrutverif@gmail.com", "verif123456789");
                client.Send(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            return 1;
        }

        public ActionResult ListRequestRegister()
        {
            List<EntrepriseModel> list = new List<EntrepriseModel>();
            List<Entreprise> listent = new List<Entreprise>();
            foreach (var item in serviceEntreprise.RequestManagement())
            {
                //ViewBag.userId = _userApplication.GetUserId(HttpContext.User);
                EntrepriseModel activity = new EntrepriseModel();
                activity.Id = item.Id;
                activity.Nom = item.Nom;
                activity.Email = item.Email;
                activity.Address = item.Address;

                //  activity.nbrPlaces = item.nbrplace;
                list.Add(activity);
            }

            Session["Request"] = serviceEntreprise.EntrepriseRequest().ToString();
            return View(list);
        }
        public ActionResult ListAcceptedRegister()
        {
            List<EntrepriseModel> list = new List<EntrepriseModel>();
            List<Entreprise> listent = new List<Entreprise>();
            foreach (var item in serviceEntreprise.listAcceptedRegister())
            {
                //ViewBag.userId = _userApplication.GetUserId(HttpContext.User);
                EntrepriseModel activity = new EntrepriseModel();
                activity.Id = item.Id;
                activity.Nom = item.Nom;
                activity.Email = item.Email;
                activity.Address = item.Address;
                //  activity.nbrPlaces = item.nbrplace;
                list.Add(activity);
            }

            Session["Request"] = serviceEntreprise.EntrepriseRequest().ToString();
            return View(list);
        }



        // POST: Admin/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult ColumnChart()
        {
            return View();
        }

        public ActionResult PieChart()
        {
            return View();
        }

        public ActionResult LineChart()
        {
            return View();
        }

        public ActionResult VisualizeStudentResult()
        {
            return Json(Result(), JsonRequestBehavior.AllowGet);
        }

        public List<Entreprise> Result()
        {
            List<Entreprise> stdResult = new List<Entreprise>();
            foreach(var item in serviceEntreprise.GetMany())
            {
                Entreprise e = new Entreprise();
                int? id = (int?)item.Id;
                    e.AccessFailedCount =serviceActivity.GetMany().Where(ac=>ac.enterpriseId==id).Count();
                
                e.Nom = item.Nom;
                stdResult.Add(e);
            }
          
            return stdResult;
        }
    }
}

