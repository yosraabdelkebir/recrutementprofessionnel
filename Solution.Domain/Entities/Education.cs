﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Solution.Domain.Entities
{
    public class Education
    {
        [Key]
        public int Id { get; set; }
        public string diploma { get; set; }
        [Required]
        public string institution { get; set; }
        public string description { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Start Date : ")]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public int? CandidatId { get; set; }
        [ForeignKey("CandidatId")]
        public virtual Candidate candidat { get; set; }
    }
}