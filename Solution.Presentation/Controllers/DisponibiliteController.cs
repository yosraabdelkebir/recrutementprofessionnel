﻿using Solution.Domain.Entities;
using Solution.Presentation.Models;
using Solution.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Solution.Presentation.Controllers
{
    public class DisponibiliteController : Controller
    {
        public static int idConnect = 5;
        IDisponibiliteService candidateService = null;
        ICandidatureService applicationSercvice = null;
        public DisponibiliteController()
        {
            candidateService = new DisponibiliteService();
            applicationSercvice = new CandidatureService();

        }
        // GET: Disponibilite
        public ActionResult Index()
        {
            var candidats = new List<DisponibiliteVM>();
            foreach (Disponibilite cdvm in candidateService.GetMany())
            {
                candidats.Add(new DisponibiliteVM()
                {
                    Id = cdvm.Id,
                    datedis = cdvm.datedis
                    
                });
            }
            return View(candidats);
        }

        // GET: Disponibilite/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Candidature/Create
        public ActionResult Create()
        {

            return View();
        }

        // POST: Disponibilite/Create
        [HttpPost]
        public ActionResult Create(DisponibiliteVM d)
        {
            Disponibilite CandiBD = new Disponibilite()
            {
                Id = d.Id,
                datedis = d.datedis,
             };
            candidateService.Add(CandiBD);
            candidateService.Commit();

            return View();
        }

        public ActionResult mail(int id)
        {
            GMailer.GmailUsername = "sarah.belkadhi@esprit.tn";
            GMailer.GmailPassword = "toutouwhisky";

            GMailer mailer = new GMailer();
            mailer.ToEmail = applicationSercvice.GetById(id).email;
            mailer.Subject = "Find a job - New Candidate Account";
            mailer.Body = "Thanks for Registering your account, Welcome to Find Me A Job!";
            mailer.IsHtml = true;
            mailer.Send();
            return RedirectToAction("Create", new { id = id });
        }

        // GET: Disponibilite/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == null)

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Disponibilite cdm = candidateService.GetById(id);
            DisponibiliteVM cvm = new DisponibiliteVM()
            {
                Id = cdm.Id,
                datedis = cdm.datedis,
                
            };
            if (cdm == null)
                return HttpNotFound();
            return View(cvm);
        }

        // POST: Disponibilite/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, DisponibiliteVM cvm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (id == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Disponibilite cdm = candidateService.GetById(id);


                    cdm.Id = cvm.Id;
                    cdm.datedis = cvm.datedis;
                   

                    if (cdm == null)
                        return HttpNotFound();

                    candidateService.Update(cdm);
                    candidateService.Commit();

                    return RedirectToAction("index", "Disponibilite", new { id = DisponibiliteController.idConnect });
                }

                return View();

            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        // GET: Disponibilite/Delete/5
        public ActionResult Delete(int id)
        {
            if (id == null)

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Disponibilite cdm = candidateService.GetById(id);
            DisponibiliteVM cvm = new DisponibiliteVM()
            {
                Id = cdm.Id,
            datedis = cdm.datedis,
            };
            if (cdm == null)
                return HttpNotFound();
            return View(cvm);
        }

        // POST: Disponibilite/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (id == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Disponibilite cdm = candidateService.GetById(id);


                    if (cdm == null)
                        return HttpNotFound();

                    candidateService.Delete(cdm);
                    candidateService.Commit();

                    return RedirectToAction("Index");
                }

                return View();

            }
            catch
            {
                return RedirectToAction("Details", new { id = DisponibiliteController.idConnect });
            }
        }
    }
}
