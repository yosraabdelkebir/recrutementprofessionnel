﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Service.Pattern;
using Solution.Data.Infrastructure;
using Solution.Domain.Entities;

namespace Solution.Service
{
    public class CandidateService : Service<Candidate>, ICandidateService
    {
        static IDataBaseFactory factory = new DataBaseFactory();
        static IUnitOfWork utk = new UnitOfWork(factory);
        public CandidateService() : base(utk)
        {

        }

        public IEnumerable<Candidate> GetCandidateById(int Id)
        {
            return GetMany(c => c.Id == Id);
        }
    }
}
