﻿using Service.Pattern;
using Solution.Data.Infrastructure;
using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Service
{
    public class ActivityService: Service<Activite>,IActivityService
    {
        private static IDataBaseFactory dbf = new DataBaseFactory();
        private static IUnitOfWork ut = new UnitOfWork(dbf);
        public ActivityService() : base(ut)
        {


        }

        public void UpdateStatus(Activite act)
        {
            ut.GetRepositoryBase<Activite>().Update(act);
            ut.commit();
        }
    }
}
