﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Domain.Entities
{
  
    public class Entretien

    {
        [Key]
        public int EntretienId { get; set; }
        public DateTime dateEntretien { get; set; }
   
        
        public int? CandidatureId { get; set; }
        [ForeignKey("CandidatureId")]
        public virtual Candidature Candidature { get; set; }
    }
}
