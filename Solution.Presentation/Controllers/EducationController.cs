﻿using Solution.Domain.Entities;
using Solution.Presentation.Models;
using Solution.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Solution.Presentation.Controllers
{
    public class EducationController : Controller
    {

        IEducationService Service = null;
        public EducationController()
        {
            Service = new EducationService();
        }


        // GET: Education
        public ActionResult Index()
        {
            //return View(Service.GetMany());
            var educations = new List<EducationVM>();
            foreach (Education edomain in Service.GetMany())
            {
                educations.Add(new EducationVM()
                {
                    Id = edomain.Id,
                    diploma = edomain.diploma,
                    institution = edomain.institution,
                    description = edomain.description,
                    startDate = edomain.startDate,
                    endDate = edomain.endDate,
                    CandidatId = edomain.CandidatId
                });
            }
            return View(educations);
        }

        // GET: Education/Details/5
        public ActionResult Details(int id)
        {
            Education e = Service.GetById(id);
            if (e == null)
            {
                return HttpNotFound();
            }

            return View(e);
        }

        // GET: Education/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Education/Create
        [HttpPost]
        public ActionResult Create(int id,EducationVM evm)
        {
            int result = DateTime.Compare(evm.startDate, evm.endDate);

            if(result==0 || result>0)
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
             
            Education educationdomain = new Education()
            {
                diploma = evm.diploma,
                institution = evm.institution,
                description = evm.description,
                startDate = evm.startDate,
                endDate = evm.endDate,
                CandidatId = id

            };
            Service.Add(educationdomain);
            Service.Commit();

            return RedirectToAction("Details", "Candidate", new { id = CandidateController.idConnect });

        }

        // GET: Education/Edit/5
        public ActionResult Edit(int id)
        {

            if (id.Equals(0)) 
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Education edm = Service.GetById(id);
            if (edm == null)
                return HttpNotFound();
            EducationVM evm = new EducationVM()
            {
                Id = edm.Id,
                diploma = edm.diploma,
                institution = edm.institution,
                description = edm.description,
                startDate = edm.startDate,
                endDate = edm.endDate,
                CandidatId = edm.CandidatId
            };

            
            return View(evm);
        }

        // POST: Education/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EducationVM evm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (id==null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    int result = DateTime.Compare(evm.startDate, evm.endDate);

                    if (result == 0 || result > 0)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Education edm = Service.GetById(id);

                    edm.diploma = evm.diploma;
                    edm.institution = evm.institution;
                    edm.description = evm.description;
                    edm.startDate = evm.startDate;
                    edm.endDate = evm.endDate;
                    edm.CandidatId = evm.CandidatId;

                    if (edm == null)
                        return HttpNotFound();

                    Service.Update(edm);
                    Service.Commit();

                    return RedirectToAction("Details", "Candidate", new { id = CandidateController.idConnect });
                }

                return RedirectToAction("Details", "Candidate", new { id = CandidateController.idConnect });

            }
            catch
            {
                return RedirectToAction("Details", "Candidate", new { id = CandidateController.idConnect });
            }
        }

        // GET: Education/Delete/5
        public ActionResult Delete(int id)
        {
            if (id==null)

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Education edm = Service.GetById(id);
            EducationVM evm = new EducationVM()
            {
                Id = edm.Id,
                diploma = edm.diploma,
                institution = edm.institution,
                description = edm.description,
                startDate = edm.startDate,
                endDate = edm.endDate,
                CandidatId = edm.CandidatId
            };
            if (edm == null)
                return HttpNotFound();
            return View(evm);

        }

        // POST: Education/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (id==null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Education edm = Service.GetById(id);


                    if (edm == null)
                        return HttpNotFound();

                    Service.Delete(edm);
                    Service.Commit();

                    return RedirectToAction("Details", "Candidate", new { id = CandidateController.idConnect });
                }

                return View();

            }
            catch
            {
                return View();
            }
        }
    }
}
