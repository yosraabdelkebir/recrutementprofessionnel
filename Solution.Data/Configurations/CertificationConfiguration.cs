﻿using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Data.Configurations
{
    public class CertificationConfiguration : EntityTypeConfiguration<Certification>
    {
        public CertificationConfiguration()
        {

            HasOptional(cer => cer.candidat).
                WithMany(cand => cand.Certifications).
                HasForeignKey(cer => cer.CandidatId).
                WillCascadeOnDelete(true);
        }
    }
}
