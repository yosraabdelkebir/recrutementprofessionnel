﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Solution.Presentation.Models
{
    public class ExperienceVM
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string workPlace { get; set; }
        [Required]
        public string campany { get; set; }
        public string description { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime startDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime endDate { get; set; }
        public int? CandidatId { get; set; }
    }
}