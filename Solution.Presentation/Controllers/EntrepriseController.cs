﻿using Solution.Data;
using Solution.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webuser.Models;

namespace webuser.Controllers
{
    public class EntrepriseController : Controller
    {
        private MyContext db;
        IActivityService serviceActivity = new ActivityService();
        IEntrepriseService serviceEntreprise = new EntrepriseService();
        // GET: Entreprise
        public ActionResult Index()
        {
            return View();
        }

        // GET: Entreprise/Details/5
        public ActionResult Details(int id)
        {
            Entreprise item = serviceEntreprise.GetById(id);
            EntrepriseModel activity = new EntrepriseModel();
            activity.Id = item.Id;
            activity.Nom = item.Nom;
            activity.Email = item.Email;
            activity.Address = item.Address;
            activity.fondateur = item.fondateur;
            
            return View();
        }

        // GET: Entreprise/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Entreprise/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Entreprise/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Entreprise/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Entreprise/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Entreprise/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
