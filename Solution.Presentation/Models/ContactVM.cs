﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Solution.Presentation.Models
{
    public class ContactVM
    {
        [Key, Column(Order = 0)]
        public int SenderId { get; set; }
        [Key, Column(Order = 1)]
        public int ReceiverId { get; set; }
        [Key, Column(Order = 2)]
        public DateTime DateEnvoie { get; set; }
        public int accept { get; set; } = 0;
    }
}